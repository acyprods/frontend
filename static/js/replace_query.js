function replace_query() {
	let url = new URL(window.location.href);
	let q = url.searchParams.get("q");
	q = document.getElementById("search_input").value;
	if (q) {
		console.log("changing query... q = " + q);
		url.searchParams.append('q', q);
		url.searchParams.set('q', q);
		window.location.assign(url.href);
	} else {
		console.log('changing query canceled. q = ' + q);
	}

	return false;
}
	