function main() {
	let current_url = new URL(window.location.href);
	let q = current_url.searchParams.get("q");
	if (q) {
		console.log("query exists, executing search()...");
		search(q);
	}
}
