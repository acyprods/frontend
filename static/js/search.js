function search(q) {
    console.log('searching... q = ' + q);
    document.getElementById("search_input").value = q;
    document.body.innerHTML += '<div style="margin:50px;padding:0" id="search">    <div><h2>Search Results</h2>        <div id="ires">            <div id="rso">                <div class="sh-sr__shop-result-group">                    <div class="sh-pr__product-results"></div>                </div>            </div>        </div>    </div>';
    request_api(q);

}

function request_api(q) {
    console.log('requesting api...');
    document.getElementById("search_input").value = q;
    let api_url = 'http://localhost:8080/api/search?q=' + q;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.response);
            parse_results(response);
        }

    };

    xmlhttp.open("GET", api_url, true);
    xmlhttp.send();

}

function parse_results(response) {
    var x = 0;
    for (x in response.records) {
        let name = response.records[x].name;
        let price = response.records[x].price;
        let brand = response.records[x].brand;
        let description = response.records[x].description;
        let template = `<div class="sh-dlr__list-result">    <div class="sh-dlr__thumbnail" style="width:180px">        <div class="_uyj _PBj">            <div class="_zyj _placeholder"></div>        </div>    </div>    <div class="_eit">        <div class="_Tav"><a data-what="1" href="">${name}</a></div>        <div class="_Bet _jjw">            <div class="_eUv">                <div><span class="_Fet">${price}</span>${brand}</div>            </div>        </div>        <div class="_Bet">${description}</div>    </div></div>`;
        let results = document.getElementsByClassName('sh-pr__product-results')[0];
        results.appendChild(htmlToElement(template));
        console.log('response: ' + response.records[x].name);
    }
}

function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim();
    template.innerHTML = html;
    return template.content.firstChild;
}